﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttons : MonoBehaviour
{

    private Color hoverColor = new Color(0.9f, 0.9f, 0.9f, 0.5f);
    private Color basicColor = new Color(1, 1, 1, 1);
    private Renderer renderer;

    void Start()
    {
        renderer = GetComponent<Renderer>();
        renderer.material.color = basicColor;
    }

    void OnMouseEnter()
    {
        renderer.material.color = hoverColor;
    }

    void OnMouseExit()
    {
        renderer.material.color = basicColor;
    }
}